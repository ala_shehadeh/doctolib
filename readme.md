##Availability test

The code exists at "src" folder, I restructured the code and created new different unit tests to cover the changes at the code.

**The code algorithm is as the following:**

* Create global variable "availabilities" so it can be accessed in all functions and output it in the end.
* Get the opening time slots from the database, insert it at "availabilities" variable in the following format

```javascript
{
    date: "2018-11-04",
    slots: ["10:30","11:00"]
}
```

* The system will check if there are recurring time slots, add the dates and time slots at "availabilities" variable depending on "numberOfDays" variable.
The system will check if "numberOfDays" is less than or equal to 7 as well so it can give accurate recurring results as well.
* The system will merge between duplicate dates and time slots in one element in case there is a duplicate in the database.
* then the system will get the appointment time from the database, check if the date and time exist at the "availabilities" variable and remove it if found. 

**Test cases**
* case 1: insert into database one day with multi opening time slots and request to get the slots for single day
* case 2: insert into database one day with multi opening time slots and request to get the slots for multi days
* case 3: insert into database many days with multi opening duplicate time slots, appointments time slots and request to get the slots for many days

##API Absence management system

The code exists at "api" folder, I used M*C with services layer for this project.
I used "JS express" for JS server and api routing, the routes exist at "server.js" file.

> PS: services layer pattern is not recommended for small projects like this one, my goal for adding it at this project is to show you how I work usually on big scale projects as I used to work on MVC with services and repositories layers

To run the application, run **yarn** to install the required dependencies, run **yarn test** for database seeding then run **yarn start** to run the server


**API documentation**

For API documentation and testing I usually use postman app, you can download it from here: 
[postman app](https://www.getpostman.com/apps)

Import the api routing and descripition from "doctolib.postman_collection.json" file, it exists at the root folder at this repository.

The APIs at the system are as the following:
* delete api/v1/:id : the system will check if the absence exists in the database and delete it if found, if it doesn't find it, it will return 404 header status and give you error message
* post api/v1/add : add absence to database after checking the body request, the body request has to be as the following:
```javascript
{	
	"type": "team event", // required has to select one of the following ('team event','person','holiday')
	"absence_type": "vacation",// required has to select one of the following ('leave','vacation')
	"starts_at": "2018-05-10", //required has to be in "YYYY-MM-DD" format for the vacation absence type or "YYYY-MM-DD HH:mm" for leave absence type 
	"ends_at": "2018-05-12", //required has to be in "YYYY-MM-DD" format for the vacation absence type or "YYYY-MM-DD HH:mm" for leave absence type 
	"group": "A", //required for group type
	"id": "1" // required for person type
}
```
if there are any errors, it will return the error messages in 404 header status
* post api/v1/search: get the stored absences from the database, depending on the income filters from the body request, the request should be as the following:
```javascript
  {
    "from": "2018-01-01",//required and accept two formats "YYYY-MM-DD" and "YYYY-MM-DD HH:mm"
    "to": "2018-12-12",//required and accept two formats "YYYY-MM-DD" and "YYYY-MM-DD HH:mm"
    "type": "holiday",//optional but if selected has to be one of the following ('holiday','person','team event')
    "absence_type": "vacation"//optional but if selected has to be one of the following ('vacation','leave')
  }
```
if there are any errors, it will return the error messages in 404 headers
