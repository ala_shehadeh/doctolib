exports.up = function (knex,Promise) {
  return Promise.all([
      knex.schema.createTable('events', table => {
    table.increments()
    table.dateTime('starts_at').notNullable()
    table.dateTime('ends_at').notNullable()
    table.enum('kind', ['appointment', 'opening']).notNullable()
    table.boolean('weekly_recurring')
  }),

      //create employees table
      knex.schema.createTable('employees', table => {
          table.increments()
          table.varchar('name',255).notNullable()
          table.varchar('group',50).notNullable()
      }),
      //create absence table
      knex.schema.createTable('absence', table => {
          table.increments()
          table.enum('type', ['holiday','team event','person']).notNullable()
          table.enum('absence_type', ['leave','vacation']).notNullable()
          table.dateTime('starts_at').notNullable()
          table.dateTime('ends_at').notNullable()
          table.json('details').notNullable()
      })
      ]);
}

exports.down = function (knex) {
  return knex.schema.dropTable('events')
}
