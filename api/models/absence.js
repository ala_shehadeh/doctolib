import knex from 'knexClient'

export default class absence {
    addAbsence(data) {
        return knex("absence").insert(
                {
                    type: data.type,
                    absence_type: data.absence_type,
                    starts_at: data.starts_at,
                    ends_at: data.ends_at,
                    details: JSON.stringify(data.details)
                }).into("absence")
        }
    selectedAbsence(id) {
        return knex('absence').where('id',id).first();
    }
    deleteAbsence(id) {
        return knex('absence').where('id',id).del();
    }
    absenceSearch(data) {
        let where = {}
        if(data.type)
            where['type'] =data.type
        if(data.absence_type)
            where['absence_type'] = data.absence_type

        return knex('absence').select()
                .whereBetween('starts_at',[data.from,data.to])
            .where(where);
    }
}