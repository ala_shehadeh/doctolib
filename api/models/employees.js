import knex from "knexClient";

export default class employees {
    allEmployees() {
        return knex
            .select("name", "group","id")
            .from("employees");
    }
    selectedGroup(group) {
        return knex
            .select("name", "group","id")
            .from("employees")
            .where('group',group);
    }
    async selectedEmployee(id) {
        return await knex
            .select("name", "group","id")
            .from("employees")
            .where('id',id).first();
    }
}