import moment from "moment/moment";

export default class timeServices {
    dateCheck(date) {
        const check = moment(date,"YYYY-MM-DD",true);
        return check.isValid();
    }
    timeCheck(date) {
        const check = moment(date,"YYYY-MM-DD HH:mm",true);
        return check.isValid();
    }

    timeCompare(from,to) {
        from = moment(from)
        to = moment(to)
        const check = to.isSameOrAfter(from);
        return check;
    }
}