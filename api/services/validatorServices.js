import timeServices from "../services/timeServices";

export default class validatorServices {
    constructor() {
        this.type = new Array('holiday','person','team event')
        this.leave_type = new Array('vacation','leave')
        this.timeServices = new timeServices();
    }
    checkLeaveType(type) {
        if(this.type.indexOf(type) >= 0)
            return true;
        else
            return false;
    }
    checkAbsenceType(type) {
        if(this.leave_type.indexOf(type) >= 0)
            return true;
        else
            return false;
    }

    searchValidation(data) {
        let error = new Array();
        if(!this.timeServices.dateCheck(data.from) && !this.timeServices.timeCheck(data.from))
            error.push('The from date/time has to be in YYYY-MM-DD or YYYY-MM-DD HH:mm format')
        if(!this.timeServices.dateCheck(data.to) && !this.timeServices.timeCheck(data.to))
            error.push('The to date/time has to be in YYYY-MM-DD or YYYY-MM-DD HH:mm format')

        if(data.absence_type) {
            if(!this.checkAbsenceType(data.absence_type))
                error.push('You have to select vacation or leave type only')
        }
        if(data.type) {
            if(!this.checkLeaveType(data.type))
                error.push('You have to select holiday, person or team event absence type only')
        }
        return error;
    }

    async leaveDataValidation(data) {
        let error = new Array()

        //check if start time smaller than end time
        if(!this.timeServices.timeCompare(data.starts_at,data.ends_at))
            error.push('the leave/vacation start time has to be smaller than end time')

        //check vacation type
        if(!this.checkLeaveType(data.type))
            error.push('the absence type is wrong')

        //check absence type
        if(!this.checkAbsenceType(data.absence_type))
            error.push('the vacation/leave type is wrong')

        if(data.absence_type == 'vacation') {
            if(!this.timeServices.dateCheck(data.starts_at))
                error.push('start date is wrong, should be YYYY-MM-DD format')
            if(!this.timeServices.dateCheck(data.ends_at))
                error.push('end date is wrong, should be YYYY-MM-DD format')
        }
        if(data.absence_type == 'leave') {
            if(!this.timeServices.timeCheck(data.starts_at))
                error.push('start leave is wrong, should be YYYY-MM-DD HH:mm format')
            if(!this.timeServices.timeCheck(data.ends_at))
                error.push('end date is wrong, should be YYYY-MM-DD HH:mm format')
        }

        //check the gorup
        if(data.type == 'team event') {
            if(!data.group)
                error.push('You have to select the employees group')
        }

        return error;
    }
}