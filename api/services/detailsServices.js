import employees from '../models/employees'
export default class detailsServices {
    constructor() {
        this.employees = new employees()
    }
    async detailsOutput(data) {
        if(data.type === 'holiday')
            return this.detailsHolidayOutput();
        else if(data.type == 'team event')
            return await this.detailsGroupOutput(data)
        else if(data.type == 'person')
            return await this.detailsEmployeeOutput(data)
    }
    detailsHolidayOutput() {
        return {
            details: {},
            employees: true
        }
    }
    async detailsGroupOutput(data) {
        let details = {}
        if(!data.group)
            details.error = 'You have to select the employees group'
        else {
            const group = await this.employees.selectedGroup(data.group);
            if(group.length > 0) {
                details.employees = true;
                details.group = data.group
            }
        }
        return details;
    }

    async detailsEmployeeOutput(data) {
        let details = {}
        if(!data.id)
            details.error = 'You have to select the employee who has the leave/vacation'
        else {
            const id = await this.employees.selectedEmployee(data.id)
            if(id) {
                details.employees = true;
                details.employee = id.id;
            }
        }
        return details;
    }
}