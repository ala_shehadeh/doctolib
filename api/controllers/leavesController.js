import absence from '../models/absence'
import detailsServices from '../services/detailsServices'
import validatorServices from '../services/validatorServices'
import employees from '../models/employees'

 class leavesController {
    constructor() {
        this.absence = new absence();
        this.detailsServices = new detailsServices();
        this.validatorServices = new validatorServices();
        this.employees = new employees()

    }

    async addLeave(data,res) {
        var employees = false;

        //data validation
        const error = await this.validatorServices.leaveDataValidation(data)
        if(error.length > 0) {
            return res.status(404).send({error:error})
        }
        else {
            const details = await this.detailsServices.detailsOutput(data)

            //add to database
            if(details.employees) {
                data.details = details;
                await this.absence.addAbsence(data);
                return res.send({success: true});
            }
            else
                return res.status(404).send(['No employees for the selected vacation/leave'])
        }
    }

    async deleteAbsence(res,req) {
        const id = await this.absence.selectedAbsence(req.params.id);
        if(id) {
            await this.absence.deleteAbsence(id.id)
            return res.send({success: true})
        }
        else
            return res.status(404).send({error: "the absence not exist at the database"});

    }

    async leavesSearch(data,res) {
        const error = this.validatorServices.searchValidation(data)

        if(error.length > 0)
            return res.status(404).send({error: error})
        else {
            const o = await this.absence.absenceSearch(data)
            const output = await this.searchOutput(o)
            return res.send(output)
        }
    }

    async searchOutput(leaves) {
        let output = new Array()
        for(let leave of leaves) {
            const details = JSON.parse(leave.details)
            if(leave.type == 'holiday')
                leave.employees = await this.employees.allEmployees();
            else if(leave.type == 'team event')
                leave.employees = await this.employees.selectedGroup(details.group)
            else if(leave.type == 'person')
                leave.employees = await this.employees.selectedEmployee(details.employee)

            output.push(leave)
        }
        return output;
    }
}
export default leavesController