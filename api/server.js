import express from 'express'
import bodyParser from 'body-parser'
import leavesController from './controllers/leavesController'

const app = express()
const PORT = 1984

const leaves = new leavesController()

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());

app.listen(PORT, () => {
    console.log(`you are server is running on ${PORT}`);
})

app.post('/api/v1/add', function (req,res) {
    return leaves.addLeave(req.body,res);
});

app.delete('/api/v1/delete/:id', function (req,res) {
    return leaves.deleteAbsence(res,req);
});

app.post('/api/v1/search', function (req,res) {
    return leaves.leavesSearch(req.body,res);
});