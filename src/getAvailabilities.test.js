import knex from "knexClient";
import getAvailabilities from "./getAvailabilities";

describe("getAvailabilities", () => {
  beforeEach(() => knex("events").truncate());
    beforeEach(() => knex("employees").truncate());

    //add employees for API test
    beforeEach(async () => {
        //add employees
        await knex('employees').insert(
            {
                name: 'Ala Shehadeh',
                group: 'A'
            })
    });

  describe("single day input without booked appointments", () => {
      beforeEach(async () => {
          await knex("events").insert([
              {
                  kind: "opening",
                  starts_at: new Date("2014-08-11 10:30"),
                  ends_at: new Date("2014-08-11 11:30"),
                  weekly_recurring: true
              }
          ]);
      });
    it("output slots for single day", async () => {
      const days = 1
      const availabilities = await getAvailabilities(new Date("2014-08-11"),days);
        expect(availabilities).toEqual([
            {
                date: '2014-08-11',
                slots: [
                    "10:30",
                    "11:00"
                ]
            }]);
    });
      it("output slots for many days", async () => {
          const days = 2
          const availabilities = await getAvailabilities(new Date("2014-08-11"),days);
          expect(availabilities).toEqual([
              {
                  date: '2014-08-11',
                  slots: [
                      "10:30",
                      "11:00"
                  ]
              },{
                  date: '2014-08-12',
                  slots: [
                      "10:30",
                      "11:00"
                  ]
              }]);
      });
  });

    describe("many days input with booked appointments", () => {
        beforeEach(async () => {
            await knex("events").insert([
                {
                    kind: "opening",
                    starts_at: new Date("2014-08-11 10:30"),
                    ends_at: new Date("2014-08-11 13:30"),
                    weekly_recurring: true
                },
                {
                    kind: "opening",
                    starts_at: new Date("2014-08-11 13:00"),
                    ends_at: new Date("2014-08-11 14:30"),
                    weekly_recurring: true
                },
                {
                    kind: "appointment",
                    starts_at: new Date("2014-08-11 10:30"),
                    ends_at: new Date("2014-08-11 11:30"),
                    weekly_recurring: false
                }
            ]);
        });
        it("output slots for single day with duplicate entries and booked appointment", async () => {
            const days = 1
            const availabilities = await getAvailabilities(new Date("2014-08-11"),days);
            expect(availabilities[0].slots).toEqual([
                        "11:30",
                        "12:00",
                        "12:30",
                        "13:00",
                        "13:30",
                        "14:00"
            ]);
        });
    });
});
