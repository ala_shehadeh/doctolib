import moment from "moment";
import knex from "knexClient";

//define availabilities as global const
const availabilities = new Array();
/**
 * get opening time from the database
 * @param from
 * @param to
 * @returns array
 */
function getOpeningData(from) {

    return knex
        .select("starts_at", "ends_at", "weekly_recurring")
        .from("events")
        .where("starts_at",'>', from)
        .where('kind','opening');
}
/**
 * get appointment time from the database
 * @param from
 * @param to
 * @returns array
 */
function getAppointmentData(from) {
    return knex
        .select("starts_at", "ends_at", "weekly_recurring")
        .from("events")
        .where("starts_at",'>', from)
        .where('kind','appointment');
}


/**
 * remove appointment time from opening array
 * @param date
 * @param time
 */
function removeAppointmentSlots(date,time){
  for (let i=0; i < availabilities.length; i++) {
    if (availabilities[i].date === date) {
      for(let k=0;k<availabilities[i].slots.length;k++) {
        if(availabilities[i].slots[k] === time)
            availabilities[i].slots.splice(k,1);
      }
    }
  }
  return availabilities;
}

/**
 * check if the availability date already exist to avoid duplicate data at the output array and return the key of selected date to can insert slots
 * @param date
 * @returns {number}
 */
function checkDate(date) {
    for(let i=0;i<availabilities.length;i++) {
        if(availabilities[i].date === date)
            return i;
    }
    return -1;
}

/**
 * remove duplicate elements of an array and return as unique elements array
 * @param arr
 * @returns {arrsay}
 */
function uniqueArray(arr){
    let unique_array = arr.filter(function(elem, index, self) {
        return index == self.indexOf(elem);
    });
    return unique_array;
}

function removeDuplicateSlots() {
    for(let i=0;i<availabilities.length;i++) {
        availabilities[i].slots = uniqueArray(availabilities[i].slots)
    }
}

function addRecurringDays(date,days,event) {
    //avoid recurring more than one week
    if(days > 7)
        days = 7

    for(let i=1;i<days;i++) {
        var key = availabilities.length
        var day = moment(date).add(i,'days').format('YYYY-MM-DD');
        const check = checkDate(day)
        if(check < 0)
            availabilities.push({date: day, slots: []})
        else
            key = check;

        //add time slots
        addTimeSlots(event,key)
    }
}


function addTimeSlots(event,key) {
    const day = moment(event.starts_at).format('YYYY-MM-DD')
    for (
        let date = moment(event.starts_at);
        date.isBefore(event.ends_at);
        date.add(30, "minutes")
    ) {
            availabilities[key].slots.push(date.format("H:mm"));
    }
}
async function removeAppointmentTimes(date) {
    const events = await
        getAppointmentData(date);

    for (const event of events) {
        const day = moment(event.starts_at).format('YYYY-MM-DD')
        //add time slots
        for (
            let date = moment(event.starts_at);
            date.isBefore(event.ends_at);
            date.add(30, "minutes")
        ) {
            removeAppointmentSlots(day,date.format("H:mm"))
        }
    }
}

export default async function getAvailabilities(date,  numberOfDays = 7) {
    const events = await
        getOpeningData(date);

  for (const event of events) {
      const day = moment(event.starts_at).format('YYYY-MM-DD')
        var key = availabilities.length
          var check = checkDate(day)
          if(check < 0)
              availabilities.push({date: day, slots: []})
          else
              key = check

          //add time slots
          addTimeSlots(event,key)

          //add recurring time slots
          if(event.weekly_recurring)
            addRecurringDays(day,numberOfDays,event)
  }

  //remove duplicates slots
    removeDuplicateSlots()

    //remove registered appointments
    await removeAppointmentTimes(date)

  return availabilities
}
